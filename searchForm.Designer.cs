﻿namespace BankingApp
{
    partial class searchForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.cboField = new System.Windows.Forms.ComboBox();
            this.tblBankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.bankDataSet = new BankingApp.BankDataSet();
            this.cboOperator = new System.Windows.Forms.ComboBox();
            this.tbSearchValue = new System.Windows.Forms.TextBox();
            this.findButton = new System.Windows.Forms.Button();
            this.cancelSearchButton = new System.Windows.Forms.Button();
            this.tblBankTableAdapter = new BankingApp.BankDataSetTableAdapters.tblBankTableAdapter();
            this.resultsDataGridView = new System.Windows.Forms.DataGridView();
            this.containsLabel = new System.Windows.Forms.Label();
            this.activeLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.tblBankBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bankDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.resultsDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // cboField
            // 
            this.cboField.AllowDrop = true;
            this.cboField.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cboField.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboField.FormattingEnabled = true;
            this.cboField.Items.AddRange(new object[] {
            "AccountCode",
            "Name",
            "DateOpened",
            "Balance",
            "OnLineBanking",
            "BIC",
            "IBAN",
            "AccountType"});
            this.cboField.Location = new System.Drawing.Point(12, 9);
            this.cboField.Name = "cboField";
            this.cboField.Size = new System.Drawing.Size(121, 21);
            this.cboField.TabIndex = 0;
            this.cboField.SelectedIndexChanged += new System.EventHandler(this.CboField_SelectedIndexChanged);
            // 
            // tblBankBindingSource
            // 
            this.tblBankBindingSource.DataMember = "tblBank";
            this.tblBankBindingSource.DataSource = this.bankDataSet;
            // 
            // bankDataSet
            // 
            this.bankDataSet.DataSetName = "BankDataSet";
            this.bankDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // cboOperator
            // 
            this.cboOperator.AllowDrop = true;
            this.cboOperator.FormattingEnabled = true;
            this.cboOperator.Items.AddRange(new object[] {
            "=",
            "<",
            "<=",
            ">",
            ">="});
            this.cboOperator.Location = new System.Drawing.Point(139, 9);
            this.cboOperator.Name = "cboOperator";
            this.cboOperator.Size = new System.Drawing.Size(121, 21);
            this.cboOperator.TabIndex = 1;
            // 
            // tbSearchValue
            // 
            this.tbSearchValue.Location = new System.Drawing.Point(266, 9);
            this.tbSearchValue.Name = "tbSearchValue";
            this.tbSearchValue.Size = new System.Drawing.Size(100, 20);
            this.tbSearchValue.TabIndex = 2;
            // 
            // findButton
            // 
            this.findButton.Location = new System.Drawing.Point(372, 7);
            this.findButton.Name = "findButton";
            this.findButton.Size = new System.Drawing.Size(75, 23);
            this.findButton.TabIndex = 3;
            this.findButton.Text = "Find";
            this.findButton.UseVisualStyleBackColor = true;
            // 
            // cancelSearchButton
            // 
            this.cancelSearchButton.Location = new System.Drawing.Point(453, 7);
            this.cancelSearchButton.Name = "cancelSearchButton";
            this.cancelSearchButton.Size = new System.Drawing.Size(75, 23);
            this.cancelSearchButton.TabIndex = 4;
            this.cancelSearchButton.Text = "Cancel";
            this.cancelSearchButton.UseVisualStyleBackColor = true;
            // 
            // tblBankTableAdapter
            // 
            this.tblBankTableAdapter.ClearBeforeFill = true;
            // 
            // resultsDataGridView
            // 
            this.resultsDataGridView.AllowUserToAddRows = false;
            this.resultsDataGridView.AllowUserToDeleteRows = false;
            this.resultsDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.resultsDataGridView.Location = new System.Drawing.Point(13, 39);
            this.resultsDataGridView.Name = "resultsDataGridView";
            this.resultsDataGridView.ReadOnly = true;
            this.resultsDataGridView.Size = new System.Drawing.Size(515, 150);
            this.resultsDataGridView.TabIndex = 5;
            // 
            // containsLabel
            // 
            this.containsLabel.AutoSize = true;
            this.containsLabel.Location = new System.Drawing.Point(152, 12);
            this.containsLabel.Name = "containsLabel";
            this.containsLabel.Size = new System.Drawing.Size(48, 13);
            this.containsLabel.TabIndex = 6;
            this.containsLabel.Text = "Contains";
            // 
            // activeLabel
            // 
            this.activeLabel.AutoSize = true;
            this.activeLabel.Location = new System.Drawing.Point(152, 12);
            this.activeLabel.Name = "activeLabel";
            this.activeLabel.Size = new System.Drawing.Size(37, 13);
            this.activeLabel.TabIndex = 7;
            this.activeLabel.Text = "Active";
            // 
            // searchForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(539, 202);
            this.Controls.Add(this.activeLabel);
            this.Controls.Add(this.containsLabel);
            this.Controls.Add(this.resultsDataGridView);
            this.Controls.Add(this.cancelSearchButton);
            this.Controls.Add(this.findButton);
            this.Controls.Add(this.tbSearchValue);
            this.Controls.Add(this.cboOperator);
            this.Controls.Add(this.cboField);
            this.Name = "searchForm";
            this.Text = "searchForm";
            this.Load += new System.EventHandler(this.SearchForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.tblBankBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bankDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.resultsDataGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cboField;
        private System.Windows.Forms.ComboBox cboOperator;
        private System.Windows.Forms.TextBox tbSearchValue;
        private System.Windows.Forms.Button findButton;
        private System.Windows.Forms.Button cancelSearchButton;
        private BankDataSet bankDataSet;
        private System.Windows.Forms.BindingSource tblBankBindingSource;
        private BankDataSetTableAdapters.tblBankTableAdapter tblBankTableAdapter;
        private System.Windows.Forms.DataGridView resultsDataGridView;
        private System.Windows.Forms.Label containsLabel;
        private System.Windows.Forms.Label activeLabel;
    }
}