﻿namespace BankingApp
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label accountCodeLabel;
            System.Windows.Forms.Label nameLabel;
            System.Windows.Forms.Label dateOpenedLabel;
            System.Windows.Forms.Label balanceLabel;
            System.Windows.Forms.Label onLineBankingLabel;
            System.Windows.Forms.Label bICLabel;
            System.Windows.Forms.Label iBANLabel;
            System.Windows.Forms.Label accountTypeLabel;
            this.bankDataSet = new BankingApp.BankDataSet();
            this.tblBankBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.tblBankTableAdapter = new BankingApp.BankDataSetTableAdapters.tblBankTableAdapter();
            this.tableAdapterManager = new BankingApp.BankDataSetTableAdapters.TableAdapterManager();
            this.accountCodeTextBox = new System.Windows.Forms.TextBox();
            this.nameTextBox = new System.Windows.Forms.TextBox();
            this.dateOpenedDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.balanceTextBox = new System.Windows.Forms.TextBox();
            this.onLineBankingCheckBox = new System.Windows.Forms.CheckBox();
            this.bICTextBox = new System.Windows.Forms.TextBox();
            this.iBANTextBox = new System.Windows.Forms.TextBox();
            this.accountTypeTextBox = new System.Windows.Forms.TextBox();
            this.firstButton = new System.Windows.Forms.Button();
            this.previousButton = new System.Windows.Forms.Button();
            this.nextButton = new System.Windows.Forms.Button();
            this.lastButton = new System.Windows.Forms.Button();
            this.addButton = new System.Windows.Forms.Button();
            this.updateButton = new System.Windows.Forms.Button();
            this.deleteButton = new System.Windows.Forms.Button();
            this.cancelButton = new System.Windows.Forms.Button();
            this.exitButton = new System.Windows.Forms.Button();
            this.rowLabel = new System.Windows.Forms.Label();
            this.SearchButton = new System.Windows.Forms.Button();
            accountCodeLabel = new System.Windows.Forms.Label();
            nameLabel = new System.Windows.Forms.Label();
            dateOpenedLabel = new System.Windows.Forms.Label();
            balanceLabel = new System.Windows.Forms.Label();
            onLineBankingLabel = new System.Windows.Forms.Label();
            bICLabel = new System.Windows.Forms.Label();
            iBANLabel = new System.Windows.Forms.Label();
            accountTypeLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.bankDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblBankBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // accountCodeLabel
            // 
            accountCodeLabel.AutoSize = true;
            accountCodeLabel.Location = new System.Drawing.Point(12, 9);
            accountCodeLabel.Name = "accountCodeLabel";
            accountCodeLabel.Size = new System.Drawing.Size(78, 13);
            accountCodeLabel.TabIndex = 1;
            accountCodeLabel.Text = "Account Code:";
            // 
            // nameLabel
            // 
            nameLabel.AutoSize = true;
            nameLabel.Location = new System.Drawing.Point(12, 35);
            nameLabel.Name = "nameLabel";
            nameLabel.Size = new System.Drawing.Size(38, 13);
            nameLabel.TabIndex = 3;
            nameLabel.Text = "Name:";
            // 
            // dateOpenedLabel
            // 
            dateOpenedLabel.AutoSize = true;
            dateOpenedLabel.Location = new System.Drawing.Point(12, 62);
            dateOpenedLabel.Name = "dateOpenedLabel";
            dateOpenedLabel.Size = new System.Drawing.Size(74, 13);
            dateOpenedLabel.TabIndex = 5;
            dateOpenedLabel.Text = "Date Opened:";
            // 
            // balanceLabel
            // 
            balanceLabel.AutoSize = true;
            balanceLabel.Location = new System.Drawing.Point(12, 87);
            balanceLabel.Name = "balanceLabel";
            balanceLabel.Size = new System.Drawing.Size(49, 13);
            balanceLabel.TabIndex = 7;
            balanceLabel.Text = "Balance:";
            // 
            // onLineBankingLabel
            // 
            onLineBankingLabel.AutoSize = true;
            onLineBankingLabel.Location = new System.Drawing.Point(12, 115);
            onLineBankingLabel.Name = "onLineBankingLabel";
            onLineBankingLabel.Size = new System.Drawing.Size(89, 13);
            onLineBankingLabel.TabIndex = 9;
            onLineBankingLabel.Text = "On Line Banking:";
            // 
            // bICLabel
            // 
            bICLabel.AutoSize = true;
            bICLabel.Location = new System.Drawing.Point(12, 143);
            bICLabel.Name = "bICLabel";
            bICLabel.Size = new System.Drawing.Size(27, 13);
            bICLabel.TabIndex = 11;
            bICLabel.Text = "BIC:";
            // 
            // iBANLabel
            // 
            iBANLabel.AutoSize = true;
            iBANLabel.Location = new System.Drawing.Point(12, 169);
            iBANLabel.Name = "iBANLabel";
            iBANLabel.Size = new System.Drawing.Size(35, 13);
            iBANLabel.TabIndex = 13;
            iBANLabel.Text = "IBAN:";
            // 
            // accountTypeLabel
            // 
            accountTypeLabel.AutoSize = true;
            accountTypeLabel.Location = new System.Drawing.Point(12, 195);
            accountTypeLabel.Name = "accountTypeLabel";
            accountTypeLabel.Size = new System.Drawing.Size(77, 13);
            accountTypeLabel.TabIndex = 15;
            accountTypeLabel.Text = "Account Type:";
            // 
            // bankDataSet
            // 
            this.bankDataSet.DataSetName = "BankDataSet";
            this.bankDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // tblBankBindingSource
            // 
            this.tblBankBindingSource.DataMember = "tblBank";
            this.tblBankBindingSource.DataSource = this.bankDataSet;
            // 
            // tblBankTableAdapter
            // 
            this.tblBankTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.tblBankTableAdapter = this.tblBankTableAdapter;
            this.tableAdapterManager.UpdateOrder = BankingApp.BankDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // accountCodeTextBox
            // 
            this.accountCodeTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.tblBankBindingSource, "AccountCode", true));
            this.accountCodeTextBox.Location = new System.Drawing.Point(107, 6);
            this.accountCodeTextBox.Name = "accountCodeTextBox";
            this.accountCodeTextBox.Size = new System.Drawing.Size(200, 20);
            this.accountCodeTextBox.TabIndex = 2;
            // 
            // nameTextBox
            // 
            this.nameTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.tblBankBindingSource, "Name", true));
            this.nameTextBox.Location = new System.Drawing.Point(107, 32);
            this.nameTextBox.Name = "nameTextBox";
            this.nameTextBox.Size = new System.Drawing.Size(200, 20);
            this.nameTextBox.TabIndex = 4;
            // 
            // dateOpenedDateTimePicker
            // 
            this.dateOpenedDateTimePicker.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.tblBankBindingSource, "DateOpened", true));
            this.dateOpenedDateTimePicker.Location = new System.Drawing.Point(107, 58);
            this.dateOpenedDateTimePicker.MaxDate = new System.DateTime(2019, 9, 18, 0, 0, 0, 0);
            this.dateOpenedDateTimePicker.MinDate = new System.DateTime(1920, 1, 1, 0, 0, 0, 0);
            this.dateOpenedDateTimePicker.Name = "dateOpenedDateTimePicker";
            this.dateOpenedDateTimePicker.Size = new System.Drawing.Size(200, 20);
            this.dateOpenedDateTimePicker.TabIndex = 6;
            this.dateOpenedDateTimePicker.Value = new System.DateTime(2019, 9, 18, 0, 0, 0, 0);
            // 
            // balanceTextBox
            // 
            this.balanceTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.tblBankBindingSource, "Balance", true));
            this.balanceTextBox.Location = new System.Drawing.Point(107, 84);
            this.balanceTextBox.Name = "balanceTextBox";
            this.balanceTextBox.Size = new System.Drawing.Size(200, 20);
            this.balanceTextBox.TabIndex = 8;
            // 
            // onLineBankingCheckBox
            // 
            this.onLineBankingCheckBox.DataBindings.Add(new System.Windows.Forms.Binding("CheckState", this.tblBankBindingSource, "OnLineBanking", true));
            this.onLineBankingCheckBox.Location = new System.Drawing.Point(107, 110);
            this.onLineBankingCheckBox.Name = "onLineBankingCheckBox";
            this.onLineBankingCheckBox.Size = new System.Drawing.Size(200, 24);
            this.onLineBankingCheckBox.TabIndex = 10;
            this.onLineBankingCheckBox.UseVisualStyleBackColor = true;
            // 
            // bICTextBox
            // 
            this.bICTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.tblBankBindingSource, "BIC", true));
            this.bICTextBox.Location = new System.Drawing.Point(107, 140);
            this.bICTextBox.Name = "bICTextBox";
            this.bICTextBox.Size = new System.Drawing.Size(200, 20);
            this.bICTextBox.TabIndex = 12;
            // 
            // iBANTextBox
            // 
            this.iBANTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.tblBankBindingSource, "IBAN", true));
            this.iBANTextBox.Location = new System.Drawing.Point(107, 166);
            this.iBANTextBox.Name = "iBANTextBox";
            this.iBANTextBox.Size = new System.Drawing.Size(200, 20);
            this.iBANTextBox.TabIndex = 14;
            // 
            // accountTypeTextBox
            // 
            this.accountTypeTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.tblBankBindingSource, "AccountType", true));
            this.accountTypeTextBox.Location = new System.Drawing.Point(107, 192);
            this.accountTypeTextBox.Name = "accountTypeTextBox";
            this.accountTypeTextBox.Size = new System.Drawing.Size(200, 20);
            this.accountTypeTextBox.TabIndex = 16;
            // 
            // firstButton
            // 
            this.firstButton.Location = new System.Drawing.Point(11, 211);
            this.firstButton.Name = "firstButton";
            this.firstButton.Size = new System.Drawing.Size(50, 25);
            this.firstButton.TabIndex = 17;
            this.firstButton.Text = "<<";
            this.firstButton.UseVisualStyleBackColor = true;
            this.firstButton.Click += new System.EventHandler(this.FirstButton_Click);
            // 
            // previousButton
            // 
            this.previousButton.Location = new System.Drawing.Point(67, 211);
            this.previousButton.Name = "previousButton";
            this.previousButton.Size = new System.Drawing.Size(50, 25);
            this.previousButton.TabIndex = 18;
            this.previousButton.Text = "<";
            this.previousButton.UseVisualStyleBackColor = true;
            this.previousButton.Click += new System.EventHandler(this.PreviousButton_Click);
            // 
            // nextButton
            // 
            this.nextButton.Location = new System.Drawing.Point(202, 211);
            this.nextButton.Name = "nextButton";
            this.nextButton.Size = new System.Drawing.Size(50, 25);
            this.nextButton.TabIndex = 19;
            this.nextButton.Text = ">";
            this.nextButton.UseVisualStyleBackColor = true;
            this.nextButton.Click += new System.EventHandler(this.NextButton_Click);
            // 
            // lastButton
            // 
            this.lastButton.Location = new System.Drawing.Point(258, 211);
            this.lastButton.Name = "lastButton";
            this.lastButton.Size = new System.Drawing.Size(50, 25);
            this.lastButton.TabIndex = 20;
            this.lastButton.Text = ">>";
            this.lastButton.UseVisualStyleBackColor = true;
            this.lastButton.Click += new System.EventHandler(this.LastButton_Click);
            // 
            // addButton
            // 
            this.addButton.Location = new System.Drawing.Point(11, 242);
            this.addButton.Name = "addButton";
            this.addButton.Size = new System.Drawing.Size(70, 23);
            this.addButton.TabIndex = 21;
            this.addButton.Text = "Add";
            this.addButton.UseVisualStyleBackColor = true;
            this.addButton.Click += new System.EventHandler(this.AddButton_Click);
            // 
            // updateButton
            // 
            this.updateButton.Location = new System.Drawing.Point(11, 271);
            this.updateButton.Name = "updateButton";
            this.updateButton.Size = new System.Drawing.Size(70, 23);
            this.updateButton.TabIndex = 22;
            this.updateButton.Text = "Update";
            this.updateButton.UseVisualStyleBackColor = true;
            this.updateButton.Click += new System.EventHandler(this.UpdateButton_Click);
            // 
            // deleteButton
            // 
            this.deleteButton.Location = new System.Drawing.Point(87, 271);
            this.deleteButton.Name = "deleteButton";
            this.deleteButton.Size = new System.Drawing.Size(70, 23);
            this.deleteButton.TabIndex = 23;
            this.deleteButton.Text = "Delete";
            this.deleteButton.UseVisualStyleBackColor = true;
            this.deleteButton.Click += new System.EventHandler(this.DeleteButton_Click);
            // 
            // cancelButton
            // 
            this.cancelButton.Location = new System.Drawing.Point(239, 242);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(70, 23);
            this.cancelButton.TabIndex = 24;
            this.cancelButton.Text = "Cancel";
            this.cancelButton.UseVisualStyleBackColor = true;
            this.cancelButton.Click += new System.EventHandler(this.CancelButton_Click);
            // 
            // exitButton
            // 
            this.exitButton.Location = new System.Drawing.Point(238, 271);
            this.exitButton.Name = "exitButton";
            this.exitButton.Size = new System.Drawing.Size(70, 23);
            this.exitButton.TabIndex = 25;
            this.exitButton.Text = "Exit";
            this.exitButton.UseVisualStyleBackColor = true;
            this.exitButton.Click += new System.EventHandler(this.ExitButton_Click);
            // 
            // rowLabel
            // 
            this.rowLabel.AutoSize = true;
            this.rowLabel.Location = new System.Drawing.Point(137, 217);
            this.rowLabel.Name = "rowLabel";
            this.rowLabel.Size = new System.Drawing.Size(0, 13);
            this.rowLabel.TabIndex = 26;
            // 
            // SearchButton
            // 
            this.SearchButton.Location = new System.Drawing.Point(87, 242);
            this.SearchButton.Name = "SearchButton";
            this.SearchButton.Size = new System.Drawing.Size(70, 23);
            this.SearchButton.TabIndex = 27;
            this.SearchButton.Text = "Search";
            this.SearchButton.UseVisualStyleBackColor = true;
            this.SearchButton.Click += new System.EventHandler(this.SearchButton_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(321, 309);
            this.Controls.Add(this.SearchButton);
            this.Controls.Add(this.rowLabel);
            this.Controls.Add(this.exitButton);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.deleteButton);
            this.Controls.Add(this.updateButton);
            this.Controls.Add(this.addButton);
            this.Controls.Add(this.lastButton);
            this.Controls.Add(this.nextButton);
            this.Controls.Add(this.previousButton);
            this.Controls.Add(this.firstButton);
            this.Controls.Add(accountCodeLabel);
            this.Controls.Add(this.accountCodeTextBox);
            this.Controls.Add(nameLabel);
            this.Controls.Add(this.nameTextBox);
            this.Controls.Add(dateOpenedLabel);
            this.Controls.Add(this.dateOpenedDateTimePicker);
            this.Controls.Add(balanceLabel);
            this.Controls.Add(this.balanceTextBox);
            this.Controls.Add(onLineBankingLabel);
            this.Controls.Add(this.onLineBankingCheckBox);
            this.Controls.Add(bICLabel);
            this.Controls.Add(this.bICTextBox);
            this.Controls.Add(iBANLabel);
            this.Controls.Add(this.iBANTextBox);
            this.Controls.Add(accountTypeLabel);
            this.Controls.Add(this.accountTypeTextBox);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.bankDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblBankBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private BankDataSet bankDataSet;
        private System.Windows.Forms.BindingSource tblBankBindingSource;
        private BankDataSetTableAdapters.tblBankTableAdapter tblBankTableAdapter;
        private BankDataSetTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.TextBox accountCodeTextBox;
        private System.Windows.Forms.TextBox nameTextBox;
        private System.Windows.Forms.DateTimePicker dateOpenedDateTimePicker;
        private System.Windows.Forms.TextBox balanceTextBox;
        private System.Windows.Forms.CheckBox onLineBankingCheckBox;
        private System.Windows.Forms.TextBox bICTextBox;
        private System.Windows.Forms.TextBox iBANTextBox;
        private System.Windows.Forms.TextBox accountTypeTextBox;
        private System.Windows.Forms.Button firstButton;
        private System.Windows.Forms.Button previousButton;
        private System.Windows.Forms.Button nextButton;
        private System.Windows.Forms.Button lastButton;
        private System.Windows.Forms.Button addButton;
        private System.Windows.Forms.Button updateButton;
        private System.Windows.Forms.Button deleteButton;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.Button exitButton;
        private System.Windows.Forms.Label rowLabel;
        private System.Windows.Forms.Button SearchButton;
    }
}

