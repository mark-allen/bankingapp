﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BankingApp
{
    public partial class searchForm : Form
    {
        public searchForm()
        {
            InitializeComponent();
        }

        private void SearchForm_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'bankDataSet.tblBank' table. You can move, or remove it, as needed.
            this.tblBankTableAdapter.Fill(this.bankDataSet.tblBank);
            activeLabel.Visible = false;
            containsLabel.Visible = false;
            //resultsDataGridView.

        }

        private void CboField_SelectedIndexChanged(object sender, EventArgs e)
        {
            if ((string) cboField.SelectedItem == "AccountCode" || 
                (string)cboField.SelectedItem == "Name" || 
                (string)cboField.SelectedItem == "BIC" || 
                (string)cboField.SelectedItem == "IBAN" || 
                (string)cboField.SelectedItem == "AccountType")
            {
                cboOperator.Visible = false;
                cboOperator.Enabled = false;
                activeLabel.Visible = false;
                containsLabel.Visible = true;
            }
            if ((string)cboField.SelectedItem == "DateOpened")
            {
                cboOperator.Visible = true;
                cboOperator.Enabled = true;
                activeLabel.Visible = false;
                containsLabel.Visible = false;
                MessageBox.Show("please enter a date in the format dd/mm/yyyy");
            }
            if ((string)cboField.SelectedItem == "Balance")
            {
                cboOperator.Visible = true;
                cboOperator.Enabled = true;
                activeLabel.Visible = false;
                containsLabel.Visible = false;
            }
            if ((string)cboField.SelectedItem == "OnLineBanking")
            {
                cboOperator.Visible = false;
                cboOperator.Enabled = false;
                activeLabel.Visible = true;
                containsLabel.Visible = false;
            }
            
            
        }
    }
}
