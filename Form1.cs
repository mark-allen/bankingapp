﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BankingApp
{
    public partial class Form1 : Form
    {
        int currentRecord;
        int totalRecords;
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'bankDataSet.tblBank' table. You can move, or remove it, as needed.
            tblBankTableAdapter.Fill(bankDataSet.tblBank);
            currentRecord = 0;
            totalRecords = tblBankBindingSource.Count;
            writeLabels();
            
        }
        private void writeLabels()
        {
            rowLabel.Text = (currentRecord + 1) + " of " + (totalRecords);
        }
        private void FirstButton_Click(object sender, EventArgs e)
        {
            tblBankBindingSource.MoveFirst();
            currentRecord = 0;
        }

        private void PreviousButton_Click(object sender, EventArgs e)
        {
            if (currentRecord >= 1)
            {
                tblBankBindingSource.MovePrevious();
                currentRecord--;
            }
        }

        private void NextButton_Click(object sender, EventArgs e)
        {
            if (currentRecord < totalRecords)
            {
                tblBankBindingSource.MoveNext();
                currentRecord++;
            }
        }

        private void LastButton_Click(object sender, EventArgs e)
        {
            tblBankBindingSource.MoveLast();
            currentRecord = totalRecords;
        }

        private void AddButton_Click(object sender, EventArgs e)
        {
            deleteButton.Visible = false;
            tblBankBindingSource.AddNew();
            currentRecord = totalRecords;
            totalRecords++;
            writeLabels();
        }

        private void UpdateButton_Click(object sender, EventArgs e)
        {
            if (!deleteButton.Visible)
            {
                deleteButton.Visible = true;
            }
            tblBankBindingSource.EndEdit();
            tblBankTableAdapter.Update(bankDataSet.tblBank);
            tblBankTableAdapter.Fill(bankDataSet.tblBank);
            currentRecord = 0;
            totalRecords = tblBankBindingSource.Count;
            writeLabels();
        }

        private void DeleteButton_Click(object sender, EventArgs e)
        {
            tblBankBindingSource.RemoveCurrent();
            tblBankBindingSource.EndEdit();
            tblBankTableAdapter.Update(bankDataSet);
            tblBankTableAdapter.Fill(bankDataSet.tblBank);
            currentRecord = 0;
            totalRecords = tblBankBindingSource.Count;
            writeLabels();
        }

        private void CancelButton_Click(object sender, EventArgs e)
        {
            deleteButton.Visible = true;
            tblBankTableAdapter.Fill(bankDataSet.tblBank);
            currentRecord = 0;
            totalRecords = tblBankBindingSource.Count;
            writeLabels();
        }

        private void ExitButton_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void SearchButton_Click(object sender, EventArgs e)
        {
            searchForm Search = new searchForm();
            Search.Show();
        }
    }
}
